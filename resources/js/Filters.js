import Vue from 'vue'

import moment from 'moment'

Vue.filter('formatDate', (date) => {
    return moment(date).format('MMM D, YYYY')
})

Vue.filter('formatDateTime', (date) => {
    return date ? moment(date).format('MMM D, YYYY h:mm A') : "--"
})

Vue.filter('formatTime', (date) => {
    return date ? moment(date).format('h:mm A') : '--'
})

Vue.filter('formatFullDate', (date) => {
    return date ? moment(date).format('dddd MMM D, YYYY') : '--'
})

Vue.filter('formatTimeFromDate', (date) => {
    return date ? moment(date).format('h:mm A') : '--'
})

Vue.filter('timeago', (date) => {
    return moment().to(date)
})

Vue.filter('truncate', function (text, stop, clamp) {
    return text.slice(0, stop) + (stop < text.length ? clamp || '...' : '')
})