<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\FarmController;
use App\Http\Controllers\FarmerAccountController;
use App\Http\Controllers\FarmerController;
use App\Http\Controllers\IdentificationController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('LandingPage');
});


// Auth

Route::get('login', [LoginController::class, 'showLoginForm'])
        ->name('login')->middleware('guest');

Route::post('login', [LoginController::class, 'login'])
        ->name('login.attempt')->middleware('guest');

Route::post('logout', [LoginController::class, 'logout'])
        ->name('logout');

Route::get('register', [RegisterController::class, 'showRegistrationForm'])        
        ->name('register')->middleware('guest');

Route::post('register', [RegisterController::class, 'register'])        
        ->name('register.submit')->middleware('guest');


// Farmers

Route::get('farmers/dashboard', [FarmerAccountController::class, 'dashboard'])        
        ->name('farmers.dashboard')->middleware('auth');

Route::get('farmers/farms', [FarmerAccountController::class, 'farms'])        
        ->name('farmers.farms')->middleware('auth');

Route::get('farmers/services', [FarmerAccountController::class, 'services'])        
        ->name('farmers.services')->middleware('auth');

Route::get('farmers/profile', [FarmerAccountController::class, 'profile'])        
        ->name('farmers.profile')->middleware('auth');


Route::post('farmers/{farmer}', [FarmerController::class, 'update'])        
        ->name('farmers.update')->middleware('auth');


// Identifications

Route::post('identifications/{id}', [IdentificationController::class, 'update'])        
        ->name('identifications.update')->middleware('auth');


// Farms

Route::get('farmers/farms/create', [FarmController::class, 'create'])        
        ->name('farms.create')->middleware('auth');

Route::get('farmers/farms/{farm}/edit', [FarmController::class, 'edit'])        
        ->name('farms.edit')->middleware('auth');

Route::post('farmers/{farmer}/farms/store-update/{farm?}', [FarmController::class, 'storeUpdate'])        
        ->name('farms.store-update')->middleware('auth');    
