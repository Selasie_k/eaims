<?php

namespace Database\Seeders;

use App\Models\Region;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = collect(json_decode(file_get_contents(__DIR__.'/../../regionslist.json'), true));

        $regions->each(function($region, $key){

            Region::create([
                'name' => $region['name']
            ])
            ->districts()
            ->createMany(
                collect($region['districts'])
                ->map(function($district){
                    return ['name' => $district];
                })
            );
        });
    }
}
