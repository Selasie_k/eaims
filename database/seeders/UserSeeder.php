<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Paul',
            'last_name' => 'Kitsi',
            'phone' => '0542873229',
            'email' => 'kitsipaul@ymail.com',
            'password' => \Hash::make('1q2w3e4r')
        ])->assignRole('super admin');
        
        User::factory(10)->create();

        User::all()
            ->each(function($user){
                $user->assignRole('farmer');
                $user->farmerProfile()
                    ->create([
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'email' => $user->email,
                        'phone' => $user->phone,
                    ])
                    ->identification()->create();
            });
    }
}
