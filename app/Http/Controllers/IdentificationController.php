<?php

namespace App\Http\Controllers;

use App\Models\Identification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class IdentificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Identification  $identification
     * @return \Illuminate\Http\Response
     */
    public function show(Identification $identification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Identification  $identification
     * @return \Illuminate\Http\Response
     */
    public function edit(Identification $identification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Identification  $identification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Identification $identification)
    {
        $request->validate([
            'type' => 'required',
            'number' => 'required',
        ]);

        $identification->update($request->all());

        return Redirect::back()->with('success', 'Indentification updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Identification  $identification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Identification $identification)
    {
        //
    }
}
