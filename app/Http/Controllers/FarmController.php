<?php

namespace App\Http\Controllers;

use App\Models\Farm;
use Inertia\Inertia;
use App\Models\Farmer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FarmController extends Controller
{

    public function create()
    {
        return Inertia::render('Farmers/Farms/CreateEdit', [
            'farmer' => auth()->user()->farmerProfile,
            'farm' => null
        ]);
    }

    public function edit(Farm $farm)
    {
        return Inertia::render('Farmers/Farms/CreateEdit', [
            'farmer' => auth()->user()->farmerProfile,
            'farm' => $farm
        ]);
    }

    public function storeUpdate(Request $request, Farmer $farmer, Farm $farm = null)
    {
        $request->validate([
            'type' => 'required',
            'location' => 'required',
        ]);

        $farm 
            ? $farm->update($request->all()) 
            : $farmer->farms()->create($request->all());

        return Redirect::route('farmers.farms')->with('success', 'Farm data added');
    }

    public function destroy(Farm $farm)
    {
        //
    }
}
