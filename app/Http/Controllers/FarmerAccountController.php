<?php

namespace App\Http\Controllers;

use App\Models\Region;
use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FarmerAccountController extends Controller
{
    public function dashboard()
    {
        return Inertia::render('Farmers/Dashboard');
    }

    public function farms()
    {
        return Inertia::render('Farmers/Farms/Index', [
            'farms' => Auth::user()
                            ->farmerProfile
                            ->farms()
                            ->paginate()
        ]);
    }

    public function services()
    {
        return Inertia::render('Farmers/Services');
    }

    public function profile()
    {
        return Inertia::render('Farmers/Profile/Index', [
            'profile' => Auth::user()->farmerProfile,
            '_identification' => Auth::user()->farmerProfile->identification,
            '_regions' => Region::select('id', 'name')->with('districts')->get()
        ]);
    }
}
