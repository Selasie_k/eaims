<?php

namespace App\Providers;

use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerInertiaAssetVersioning();

        $this->registerInertia();
    }

    public function registerInertiaAssetVersioning()
    {
      Inertia::version(function () {
          return md5_file(public_path('mix-manifest.json'));
      });
    }


    public function registerInertia()
    {
        Inertia::share([
            'auth' => function () {
                return [
                    'user' => Auth::user() ? [
                        'id' => Auth::user()->id,
                        'name' => Auth::user()->name,
                        'email' => Auth::user()->email,
                        // 'roles' => Auth::user()->getRoleNames(),
                    ] : null,
                ];
            },
            'flash' => function () {
                return [
                    'success' => Session::get('success'),
                    'error' => Session::get('error'),
                ];
            },
            'app' => function() {
                return [
                    "name" => config('app.name')
                ];
            }
        ]);
    }
}
